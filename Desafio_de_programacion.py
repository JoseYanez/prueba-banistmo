#!/usr/bin/env python
# coding: utf-8

# # Enunciado

# #### Descripción
# Se considera que un entero positivo es perfecto si y solo si la suma de sus dígitos es exactamente 10
#
# Dado un entero positivo Z, tu tarea es encontrar el Z-avo (elemento en la posición Z) entero perfecto más pequeño.
#
# #### Entrada
# Una sola línea con un entero positivo Z
#  (1 ≤ Z ≤ 10000).
#
# #### Salida
# Un solo número, que denota el Z-avo entero perfecto más pequeño
#
# #### Ejemplos
# _Entrada_ = 1
# _Salida_ = 19
#
# _Entrada_ = 2
# _Salida_ = 28
#
# __Nota:__ El primer entero perfecto es 19 y el segundo es 28.
#

# ### IMPORTACIÓN DE LIBRERIAS

# In[152]:


import numpy
from pprint import pprint


# # Métodos compartidos

# `Métodos para casting a int en listas y casting de listas a int`

# In[63]:


def numToList(num):
    """ Función que divide un Integer en elementos de una lista
        Ejemplo: 123 ==> [1, 2, 3]
    """
    return [int(d) for d in str(num)]


# In[132]:


def listToNum(digits:list):
    """ Función que une los elementos de una lista en un Integer
        Ejemplo: [2, 0, 8] ==> '208' ==> 208
    """
    result = ''.join(str(i) for i in digits)
    return int(result)


# # Testing de función listToNum

# In[133]:


listToNum([9,1])


# # Método UNO

# In[146]:


def easyFunction(num):
    digits = numToList(num)
    if len(digits) == 1: digits.append(0)
    if sum(digits) != 10:
        # print(digits)
        num = listToNum(digits) + 1
        digits = easyFunction(num)
    return digits


# ### USO MÉTODO UNO

# Ingreso un número cualquiera y encontrará el número perfecto siguiente más próximo.

# In[147]:


easyFunction(9)


# #### USO DE MÉTODO UNO CON LISTA DE NÚMEROS, SE CREA UNA LISTA DE 0 A 10.000 ELEMENTOS PARA ITERARLOS Y ENCONTRAR EL SIGUIENTE NÚMERO PERFECTO. POR OTRA PARTE, SE UTILIZA UN OBJECTO set() PARA EVITAR VALORES DUPLICADOS. REALMENTE EL MÉTODO SE PUEDE OPTIMIZAR

# ## USO DE FUNCIÓN EJECUTADA POR UN FOREACH DE 10.000 ELEMENTOS

# In[153]:


numSet = set()
for num in range(1, 10000):
    numSet.add(listToNum(easyFunction(num)))
pprint(sorted(numSet))


# # Método DOS (Intento Fallido)

# #### INTENTE UTILIZAR UN WHILE PARA DETECTAR LOS NÚMEROS PERFECTOS, PARA SALTARME EL PROBLEMA DE LOS NIVELES DE RECURSIÓN QUE TIENE LA SOLUCIÓN 1.

# In[77]:


def numPerfecto(num):
    digits = numToList(num)
    lastIndex = len(digits) - 1

    if sum(digits) == 10:
        print('número ingresado es un entero positivo perfecto, se ha agregado ')
    #if lastIndex == 0:
    digits.append(0)
    lastIndex += 1
    print(lastIndex)
    counter = digits[lastIndex]
    while sum(digits) < 10 or counter != 10:
        counter += 1
        if counter == 0:
            digits.append(0)
            lastIndex += 1
        print(digits, sum(digits))
        if sum(digits) == 10:
            break

        digits[lastIndex] = counter
        #print(digits[lastIndex])
    return digits, listToNum(digits)


# In[154]:


print(numPerfecto(99))


# In[ ]:
