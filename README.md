# Prueba Banistmo

---------------
Autor: José Yáñez Zaraza
Fecha: 30/10/2018

Descripción: Repositorio para prueba de Banco Banistmo. Se encuentran desarrolladas ambos documentos Word (.docx)

---------------

### Cada notebook de jupyter tiene los ejercicios de un documento.

__Reto Programacion1:__ Desafío de programación.ipynb

__Prueba Centro Innovacion:__ Prueba Centro de Innovación.ipynb
